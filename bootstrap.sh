#!/bin/bash

ROS_DRONE_BOOTSTRAP_PATH=${ROS_DRONE_BOOTSTRAP_PATH:-/drone}
ROS_DRONE_CI_BRANCH=${ROS_DRONE_CI_BRANCH:-master}

if [ ! -d ${ROS_DRONE_BOOTSTRAP_PATH} ]; then
  mkdir ${ROS_DRONE_BOOTSTRAP_PATH}
fi

if [ ! -d ${ROS_DRONE_BOOTSTRAP_PATH}/ros_drone_ci ]; then
  echo "Cloning ros_drone_ci branch ${ROS_DRONE_CI_BRANCH}";
  cd ${ROS_DRONE_BOOTSTRAP_PATH} && git clone --branch ${ROS_DRONE_CI_BRANCH} --depth 1 https://gitlab.com/amarburg/ros_drone_ci.git;
else
  echo "!! Using existing ${ROS_DRONE_BOOTSTRAP_PATH}/ros_drone_ci"
fi

ROS_DRONE_PATH=${ROS_DRONE_BOOTSTRAP_PATH}/ros_drone_ci
. ${ROS_DRONE_PATH}/build.bash
