[![Build Status](https://gitlab.drone.camhd.science/api/badges/amarburg/ros_drone_ci/status.svg)](https://gitlab.drone.camhd.science/amarburg/ros_drone_ci)

Scripts to assist with testing ROS projects in a Docker pipeline in [Drone](https://drone.io).   Forked from [VictorLamoine's Gitlab CI scripts](https://gitlab.com/VictorLamoine/ros_gitlab_ci/).

Tested with ROS Melodic and Noetic.   I suggest using the [ROS Drone CI Images](https://hub.docker.com/repository/docker/amarburg/drone-ci-ros) ([at Gitlab](https://gitlab.com/amarburg/ros_drone_ci_images)) which have all of the prerequisites pre-loaded --- however, the script should correctly `apt-get install` all of the dependencies assuming you're starting from one of the official [ROS images](https://hub.docker.com/_/ros).

## Quick start

Create a `.drone.yml`:

    kind: pipeline
    type: docker
    name: build

    platform:
      os: linux
      arch: amd64

    steps:
    - name: build
      image: ros:melodic-ros-base
      commands:
        - wget -O- https://gitlab.com/amarburg/ros_drone_ci/-/raw/master/bootstrap.sh | /bin/bash

Or:

    commands:
      - if [ ! -d ros_drone_ci ]; then git clone https://gitlab.com/amarburg/ros_drone_ci.git; fi
      - ros_drone_ci/build.bash

If some additional actions needs to be taken between setting up the Catkin workspace and building, set ROS_DRONE_DONT_BUILD
before calling build.bash (or bootstrap):

    steps:
    - name: build
      image: ros:melodic-ros-base
      commands:
        - wget -O- https://gitlab.com/amarburg/ros_drone_ci/-/raw/master/bootstrap.sh | ROS_DRONE_DONT_BUILD=true /bin/bash
        - (additional commands)
        - /drone/ros_drone_ci/build.bash

## Useful variables:

 * `ROS_DRONE_DONT_BUILD` -- script sets up workspace but does not `catkin build`
 * `ROS_DRONE_DONT_TEST`  -- script sets up and builds workspace but does not `catkin run_tests`
 * `ROS_DRONE_DONT_PRECOMMIT`  -- Don't run `pre-commit`, even if `.pre-commit-config.yaml` exists in the repo
 * `ROS_DRONE_PATH` is the the location of the Catkin workspace.  Default: `/drone/catkin_workspace`
 * `ROS_PACKAGES_TO_INSTALL` (empty by default) specify extra ROS packages to install, for `ros-kinetic-rviz` just add `rviz` to the list, the ROS distro is automatically detected.
 * `GLOBAL_C11` (not defined by default), if defined to `true`: forces C++11 for every project compiled, defined it to any value (eg `true`) to globally enable C++11.
 * `DISABLE_GCC_COLORS` (false by default), if defined to `true`: disables gcc colour output ([-fdiagnostics-color](https://gcc.gnu.org/onlinedocs/gcc/Diagnostic-Message-Formatting-Options.html)).
 * `USE_ROSDEP` (true by default) use [rosdep](http://wiki.ros.org/rosdep/) to install dependencies. Define to `false` to disable.
 * `VCS_RECURSIVE` (false by default). If `true`, `vcs` will run recursively, importing every `.rosinstall` or `.repos` files in the workspace until all have been processed. For this feature to work:
   - The rosinstall file names must be unique
   - No repository may contain a space in it's name
 * `ROSINSTALL_FILE` (unset by default)  Allows `.rosinstall` file to be manually specified.

There are couple of variables which are _only used in the bootstrap script_:

 * `ROS_DRONE_BOOTSTRAP_PATH` sets the directory where the bootstrap script clones this repo.  Default: `/drone`
 * `ROS_DRONE_CI_BRANCH` sets which branch of this repo is cloned.  Default: `master`


This repository uses the [ROS Docker](https://store.docker.com/images/ros) images to compile your packages, it does not run tests by default.

# Similar projects:

 * Using Travis CI? Take a look at [ros-industrial/industrial_ci](https://github.com/ros-industrial/industrial_ci).
 * Using Gitlab pipelines?   Use [VictorLamoine's Gitlab CI scripts](https://gitlab.com/VictorLamoine/ros_gitlab_ci/).

# License

Distributed under the BSD license, see [LICENSE](LICENSE)

-----

Leftovers from the original README.md ...

## Using with [`catkin_lint`](http://wiki.ros.org/catkin_lint)
Example usage:
```yml
# catkin_lint
catkin lint:
  stage: test
  image: ros:melodic-ros-core
  needs: []
  allow_failure: true
  script:
    - catkin_lint -W3 .
```
