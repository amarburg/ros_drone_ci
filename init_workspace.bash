#!/bin/bash

# Prepare build
#--------------
# https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables


# Does this project have a wstool install file?
if [[ -z "${ROSINSTALL_FILE}" ]]; then
  rosinstall_file=$(find ${CI_PROJECT_DIR} -maxdepth 2 -type f \( -name "*rosinstall" -o -name "*.repos" \) )
else
  rosinstall_file=${CI_PROJECT_DIR}/${ROSINSTALL_FILE}
fi

rm -rf ${ROS_DRONE_WORKSPACE}/src/
echo "## Creating ${ROS_DRONE_WORKSPACE}/src/"
mkdir -p ${ROS_DRONE_WORKSPACE}/src/

if [[ -z "${rosinstall_file}" ]]; then
  # If no rosinstall file
  # Don't move the original clone or GitLab CI fails!

  echo "## Symlinking source directory ${CI_PROJECT_DIR} to ${ROS_DRONE_WORKSPACE}/src/"
  ln -s ${CI_PROJECT_DIR} ${ROS_DRONE_WORKSPACE}/src/

  # Don't need to do this...
  #if [[ "${ROS_DRONE_BUILD_ROS2}" != "true" ]]; then
  #	catkin_init_workspace .
  #fi

else
  echo "## Using file ${rosinstall_file}"

  # Install vcstool
  echo "## Installing vcstool"
  apt install -y ${PYTHON_VERSION}-vcstool

  # Create workspace
  echo "Using rosinstall file ${rosinstall_file}"

  # n.b. "--recursive" flag to vcs means to checkout any submodules
  cd ${ROS_DRONE_WORKSPACE}/src && vcs import --shallow --recursive  < ${rosinstall_file}

  if [[ "${VCS_RECURSIVE}" == "true" ]]; then
    echo "Using vcstool recursively"
    while : ; do
      rosinstall_files=$(find ${ROS_DRONE_WORKSPACE}/src -type f -name "*?.{rosinstall|repos}")
      rosinstall_file=$(echo ${rosinstall_files} | cut -d ' ' -f 1)
      if [[ -z "${rosinstall_file}" ]]; then
        break
      fi

      cd ${ROS_DRONE_WORKSPACE}/src && vcs import --shallow --recursive --skip-existing < ${rosinstall_file}
      rm ${rosinstall_file}
    done
  fi

  # If the project itself is not included in rosinstall file, copy it manually
  if [ ! -d "src/${CI_PROJECT_NAME}" ]; then
    ln -s ${CI_PROJECT_DIR} ${ROS_DRONE_WORKSPACE}/src/
  fi
fi

# Initialize git submodules
cd ${ROS_DRONE_WORKSPACE}/src
find . -type d -name ".git" -exec "cd {}/.. && git submodule init && git submodule update" \;
cd ${ROS_DRONE_WORKSPACE}

# Install packages using rosdep
if [[ ("${USE_ROSDEP}" != false && ! -z "${USE_ROSDEP}") || -z "${USE_ROSDEP}" && "${ROS_DRONE_BUILD_ROS2}" != "true" ]]; then
  echo "## Using rosdep to install dependencies"

  ls src/

  # Install rosdep and initialize
  apt-get install -qq ${PYTHON_VERSION}-rosdep ${PYTHON_VERSION}-pip ${PYTHON_VERSION}-rospkg ${PYTHON_VERSION}-osrf-pycommon

  if [ -f /etc/ros/rosdep/sources.list.d/20-default.list ]; then
    rm /etc/ros/rosdep/sources.list.d/20-default.list;
  fi
  rosdep init || true
  rosdep update --include-eol-distros

  # Use rosdep to install dependencies
  rosdep install --from-paths src --ignore-src --rosdistro ${ROS_DISTRO} -y --as-root apt:false
fi
