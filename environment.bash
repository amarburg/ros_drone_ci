#!/usr/bin/env bash

## Default values for common variables

## Variables to emulate TravisCI (for now)

CI_PROJECT_DIR=${CI_PROJECT_DIR:-/drone/src}
CI_PROJECT_NAME=${DRONE_REPO_NAME:-"this_repo"}

ROS_DRONE_WORKSPACE=${ROS_DRONE_WORKSPACE:-/drone/catkin_workspace}

# Determine ROS_DISTRO
#---------------------
ROS_DISTRO=$(ls /opt/ros/)

if [[ -z "${ROS_DISTRO}" ]]; then
  echo "No ROS distribution was found in /opt/ros/. Aborting!"
  exit 1
fi

if [[ "$ROS_DISTRO" == "melodic" ]]; then
  PYTHON_VERSION=python
else
  PYTHON_VERSION=python3
fi

## Backwards compatibility with old env variable names.
if [[ ! -z "${WSTOOL_RECURIVE}" ]]; then
  VCS_RECURSIVE=${WSTOOL_RECURSIVE}
fi

# Source ROS
#-----------
. /opt/ros/${ROS_DISTRO}/setup.bash

# Ensure apt doesn't ask any questions
export DEBIAN_FRONTEND=noninteractive
