#!/bin/bash

# Bootstrap ssh-agent, in case it's needed
apt-get update -y && apt-get install -y openssh-client
eval $(ssh-agent -s)
mkdir -pm 0700 $HOME/.ssh && ssh-keyscan github.com gitlab.com >> $HOME/.ssh/known_hosts

# Install catkin tools # https://catkin-tools.readthedocs.io/en/latest/installing.html
#---------------------
if [[ -z "/etc/apt/sources.list.d/ros-latest.list" ]]; then
  apt install -qq wget
  sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list'
  wget http://packages.ros.org/ros.key -O - | apt-key add -
fi
apt update
apt install -y -qq gcc g++ xterm ${PYTHON_VERSION}-catkin-tools ${PYTHON_VERSION}-catkin-lint
export TERM="xterm"


# Install ROS packages required by the user
#------------------------------------------
# Split packages into package list
IFS=' ' read -ra PACKAGES <<< "${ROS_PACKAGES_TO_INSTALL}"
# Clear packages list
ROS_PACKAGES_TO_INSTALL=""

# Append "ros-kinetic-" (eg for Kinetic) before the package name
# and append in the packages list
for package in "${PACKAGES[@]}"; do
  ROS_PACKAGES_TO_INSTALL="${ROS_PACKAGES_TO_INSTALL} ros-${ROS_DISTRO}-${package}"
done

# Install the packages
echo "## Installing additional ROS packages: $ROS_PACKAGES_TO_INSTALL"
echo "##  And these additional APT packages: $APT_PACKAGES_TO_INSTALL"
apt-get install -qq ${ROS_PACKAGES_TO_INSTALL} ${APT_PACKAGES_TO_INSTALL}

# Add color diagnostics
#----------------------
# Don't add if user defined the DISABLE_GCC_COLORS variable
# Don't add if gcc is too old to support the option

# http://unix.stackexchange.com/questions/285924/how-to-compare-a-programs-version-in-a-shell-script/285928#285928
gcc_version="$(gcc -dumpversion)"
required_ver="4.9.0"
if [[ "$(printf "$required_ver\n$gcc_version" | sort -V | head -n1)" == "$gcc_version" ]] && [[ "$gcc_version" != "$required_ver" ]]; then
  echo "Can't use -fdiagnostics-color, gcc is too old!"
else
  if [[ ! -z "${DISABLE_GCC_COLORS}" && "${DISABLE_GCC_COLORS}" == "true" ]]; then
    export CXXFLAGS="${CXXFLAGS} -fdiagnostics-color"
  fi
fi

# Enable global C++11 if required by the user
#--------------------------------------------
if [[ "${GLOBAL_C11}" == "true" ]]; then
  echo "Enabling C++11 globally"
  export CXXFLAGS="${CXXFLAGS} -std=c++11"
fi

# Display system information
#---------------------------
echo "##############################################"
uname -a
lsb_release -a
gcc --version
echo "CXXFLAGS = ${CXXFLAGS}"
cmake --version
echo "##############################################"

# Self testing
#-------------
if [[ "${SELF_TESTING}" == "true" ]]; then
  # We are done, no need to prepare the build
  return
fi
