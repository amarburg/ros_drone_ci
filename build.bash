#!/bin/bash
## Runs all steps

# exit when any command fails
set -e

# /drone/src/ros_drone_ci is the path you get if you just "git clone ..." in the .drone.yml
ROS_DRONE_PATH=${ROS_DRONE_PATH:-/drone/src/ros_drone_ci}

. ${ROS_DRONE_PATH}/environment.bash

# Run pre-commit before the checked-out repo gets moved
echo "Checking for .pre-commit-config.yaml"
if [ -z ${ROS_DRONE_DONT_PRECOMMIT} ];  then
  if [ -f ${CI_PROJECT_DIR}/.pre-commit-config.yaml ]; then
    echo "Running pre-commit..."
    cd ${CI_PROJECT_DIR} && pre-commit run --all-files
  fi
fi

if [ ${ROS_DRONE_BUILD_ROS2} ]; then
   . ${ROS_DRONE_PATH}/init_workspace.bash
   cd ${ROS_DRONE_WORKSPACE} && colcon build
   exit 0
fi

## TODO skip if already done...
. ${ROS_DRONE_PATH}/install_prerequisites.bash

if [ ! -d ${ROS_DRONE_WORKSPACE} ]; then
  . ${ROS_DRONE_PATH}/init_workspace.bash
fi

cd ${ROS_DRONE_WORKSPACE}

if [ ! -z ${ROS_DRONE_DONT_BUILD} ]; then exit 0; fi
catkin build --summarize --no-status --force-color

if [ ! -z ${ROS_DRONE_DONT_TEST} ]; then exit 0; fi
# This rather than "catkin run_tests" because "catkin run_tests" is
# overly verbose.
catkin test
